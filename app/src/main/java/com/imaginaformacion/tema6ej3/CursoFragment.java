package com.imaginaformacion.tema6ej3;

import android.app.Activity;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class CursoFragment extends ListFragment {

    private OnFragmentInteractionListener mListener;
    private static CursoFragment fragment;

    public static CursoFragment getInstance() {
        if (fragment == null)
            fragment = new CursoFragment();
        return fragment;
    }

    public CursoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<String> categories = new ArrayList<String>();
        categories.add("Mobile");
        categories.add("Web");
        setListAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, categories));
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mListener = (OnFragmentInteractionListener) activity;
        //TODO inicializar mListener para que referencie al Activity
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        mListener.onFragmentInteraction(position);
        //TODO Hacer la llamada a la función del listener con la posición.

    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Integer position);
    }

}
