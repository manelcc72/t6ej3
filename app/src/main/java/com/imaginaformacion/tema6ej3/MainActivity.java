package com.imaginaformacion.tema6ej3;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

//TODO MainActivity debe implementar OnFragmentInteractionListener de CursoFragment
public class MainActivity extends Activity implements CursoFragment.OnFragmentInteractionListener {

    View fragmentList;
    View fragmentContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActionBar().setTitle("Imagina Formación");

        fragmentList = findViewById(R.id.container_list);
        fragmentContent = findViewById(R.id.container_content);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        if (fragmentList != null) {
            transaction.add(fragmentList.getId(), CursoFragment.getInstance());
            transaction.add(fragmentContent.getId(), PlaceholderFragment.newInstance(0));
            //TODO Añadir CursoFragment.getInstance() a fragmentList
            //TODO Añadir PlaceholderFragment.newInstance(0) a fragmentContent
        } else {
            transaction.add(fragmentContent.getId(), CursoFragment.getInstance());
            //TODO Añadir CursoFragment.getInstance() a fragmentContent
        }

        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Integer position) {

        PlaceholderFragment fragment = PlaceholderFragment.newInstance(position);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(fragmentContent.getId(), fragment);
        if (fragmentList == null) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    //TODO Sobreescribe la función onFragmentInteraction(Integer position) y dentro haz lo siguiente:
    //TODO Crea una instancia de PlaceholderFrament enviando la posición seleccionada
    //TODO Empieza una transacción
    //TODO Remplaza el contenido de fragmentContent por fragment
    //TODO Si solo existe un contenedor, añadir al Back Stack para que funcione correctamente con el botón de Atrás.
    //TODO Confirma los cambios en el fragment

}
